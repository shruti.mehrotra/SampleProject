package com.shruti.twitter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shruti.twitter.convertor.TweetRequestConvertor;
import com.shruti.twitter.model.request.PostTweetRequest;
import com.shruti.twitter.neo4j.model.node.TweetNode;
import com.shruti.twitter.neo4j.repo.node.TweetNodeRepository;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */
@Service("TweetService")
public class TweetService {
	
	@Autowired
	TweetNodeRepository tweetNodeRepository;
	
	public boolean postTweet(PostTweetRequest request) {
		boolean tweetPosted = false;
		
		TweetNode tweetNode = TweetRequestConvertor.toNode(request);
		//Step 1: Persist the tweet for user
		tweetNode = tweetNodeRepository.save(tweetNode);
		tweetPosted = true;
		//Step 2: Send notification to stream (kafka) with a priority for async publish to users
			//Step 2(i): Kafka consumer will pick up the tweet from user 
			//and push a notification to all followers, with a priority
		
		//Step 3: Return
		
		return tweetPosted;
	}
	
	public boolean deleteTweet(PostTweetRequest request) {
		boolean tweetDeleted = false;
		
		TweetNode tweetNode = TweetRequestConvertor.toNode(request);
		//Step 1: Persist the tweet for user
		tweetNodeRepository.delete(tweetNode);
		tweetDeleted = true;
		//Step 2: Send notification to stream (kafka) with a priority for async publish to users
			//Step 2(i): Kafka consumer will pick up the tweet from user 
			//and push a notification to all followers, with a priority
		
		//Step 3: Return
		
		return tweetDeleted;
	}
}
