package com.shruti.twitter.model.types;
/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 20-Dec-2016
 */
public enum Reaction {
	VIEW, LIKE, DISLIKE, WOW, SAD, LOVE;
}
