package com.shruti.twitter.model.request;

import java.io.File;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;

import com.shruti.twitter.model.constants.SizeConstants;
import com.shruti.twitter.model.user.UserIdentity;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */
@Data
public class PostTweetRequest {
	@NotNull
	@Valid
	private UserIdentity userIdentity;
	
	@NotBlank
	@Size(max = SizeConstants.TWEET_HEADING_MAX_SIZE, message = "The field must be less than 50 characters")
	@Pattern(regexp="[^<>~!^()]*", message="heading is not valid")
	private String heading;
	
	private List<File> attachments;
	
	@NotBlank
	@Size(max = SizeConstants.TWEET_CONTENT_MAX_SIZE, message = "The field must be less than 250 characters")
	@Pattern(regexp="[^<>~!^()]*", message="content is not valid")
	private String content;
}
