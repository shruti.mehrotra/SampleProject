package com.shruti.twitter.model.tweet;

import lombok.Data;

import com.shruti.twitter.model.user.UserIdentity;
/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */
@Data
public class TweetReply {
	private String reply;
	private UserIdentity userIdentity;
}
