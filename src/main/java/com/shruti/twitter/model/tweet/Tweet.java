package com.shruti.twitter.model.tweet;

import java.io.File;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Tweet {
//	private UserIdentity userIdentity;
	private String heading;
	private List<File> attachments;
	private String content;
	private Date tweetTime;
	private List<TweetReaction> reactions;
	private List<TweetReply> replies;
//	private List<Tweet> retweets;
}
