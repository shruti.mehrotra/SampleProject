package com.shruti.twitter.model.tweet;

import lombok.Data;

import com.shruti.twitter.model.types.Reaction;
import com.shruti.twitter.model.user.UserIdentity;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 20-Dec-2016
 */
@Data
public class TweetReaction {
	private UserIdentity reactorIdentity;
	private Reaction reaction;
}
