package com.shruti.twitter.model.constants;
/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */
public class TweetApiConstants {
	public static final String POST_TWEET = "/postTweet";
	public static final String DELETE_TWEET = "/deleteTweet";
}
