package com.shruti.twitter.model.constants;
/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */
public class SizeConstants {

	public static final int TWEET_HEADING_MAX_SIZE = 50;
	public static final int TWEET_CONTENT_MAX_SIZE = 250;
	public static final int USER_EMAIL_MAX_SIZE = 50;
	public static final int USER_MOBILE_MAX_SIZE = 10;
	public static final int USER_TOKEN_MAX_SIZE = 50;
	public static final int USER_TAG_MAX_SIZE = 50;

}
