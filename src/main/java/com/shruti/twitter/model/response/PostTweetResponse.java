package com.shruti.twitter.model.response;

import lombok.Data;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */
@Data
public class PostTweetResponse {
	private boolean posted;
}
