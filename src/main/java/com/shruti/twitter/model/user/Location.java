package com.shruti.twitter.model.user;

import lombok.Data;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 20-Dec-2016
 */
@Data
public class Location {
	private String geoHash;
	private String city;
}
