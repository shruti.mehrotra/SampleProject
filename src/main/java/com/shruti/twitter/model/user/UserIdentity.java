package com.shruti.twitter.model.user;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.shruti.twitter.model.constants.SizeConstants;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 20-Dec-2016
 */
@Data
@ToString
public class UserIdentity {
	
	@NotBlank
	@Size(max = SizeConstants.USER_EMAIL_MAX_SIZE, message = "The field must be less than 50 characters")
	@Pattern(regexp="[^<>~!^()]*", message="email is not valid")
	private String email;
	
	@NotBlank
	@Size(max = SizeConstants.USER_MOBILE_MAX_SIZE, message = "The field must be less than 10 characters")
	@Pattern(regexp="[^<>~!^()]*", message="mobile is not valid")
	private String mobile;
	
	@NotBlank
	@Size(max = SizeConstants.USER_TOKEN_MAX_SIZE, message = "The field must be less than 50 characters")
	@Pattern(regexp="[^<>~!^()]*", message="token is not valid")
	private String token;

	@NotBlank
	@Size(max = SizeConstants.USER_TAG_MAX_SIZE, message = "The field must be less than 50 characters")
	@Pattern(regexp="[^<>~!^()]*", message="tagName is not valid")
	private String tagName;
}
