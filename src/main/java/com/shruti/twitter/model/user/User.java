package com.shruti.twitter.model.user;

import java.util.List;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 20-Dec-2016
 */
@Data
@ToString
public class User {
	private UserDetails userDetails;
	private UserIdentity userIdentity;
	private List<UserIdentity> followers;
	private List<UserIdentity> following;
}