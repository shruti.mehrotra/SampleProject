package com.shruti.twitter.model.user;

import java.io.File;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 20-Dec-2016
 */
@Data
@ToString
public class UserDetails {
	private String name;
	private String dob;
	private Location location;
	private String websiteLink;
	private File profilePic;
}
