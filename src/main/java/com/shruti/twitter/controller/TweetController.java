package com.shruti.twitter.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.shruti.twitter.model.constants.TweetApiConstants;
import com.shruti.twitter.model.request.PostTweetRequest;
import com.shruti.twitter.model.response.PostTweetResponse;
import com.shruti.twitter.service.TweetService;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */
@RestController("/tweet")
public class TweetController {
	
	@Autowired
	private TweetService tweetService;
	
	@RequestMapping(name = TweetApiConstants.POST_TWEET, method = RequestMethod.POST)
	public @ResponseBody PostTweetResponse postTweet(@Valid @RequestBody PostTweetRequest request) {
		PostTweetResponse response = new PostTweetResponse();
		response.setPosted(tweetService.postTweet(request));
		return response;
	}
	
	@RequestMapping(name = TweetApiConstants.DELETE_TWEET, method = RequestMethod.POST)
	public @ResponseBody PostTweetResponse deleteTweet(@Valid @RequestBody PostTweetRequest request) {
		PostTweetResponse response = new PostTweetResponse();
		response.setPosted(tweetService.deleteTweet(request));
		return response;
	}
}
