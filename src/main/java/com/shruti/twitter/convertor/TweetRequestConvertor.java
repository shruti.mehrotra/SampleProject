package com.shruti.twitter.convertor;

import java.util.Date;

import com.shruti.twitter.model.request.PostTweetRequest;
import com.shruti.twitter.model.tweet.Tweet;
import com.shruti.twitter.model.user.UserIdentity;
import com.shruti.twitter.neo4j.model.node.TweetNode;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */
public class TweetRequestConvertor {
	public static TweetNode toNode(PostTweetRequest request) {
		TweetNode tweetNode = new TweetNode();
		Tweet tweet = new Tweet();
		tweet.setHeading(request.getHeading());
		tweet.setContent(request.getContent());
		tweet.setTweetTime(new Date());
		tweet.setAttachments(request.getAttachments());
//		tweetNode.setTweet(tweet );
		tweetNode.setContent(request.getContent());
		tweetNode.setHeading(request.getHeading());
		tweetNode.setTweetTime(new Date());
		tweetNode.setAttachments(request.getAttachments());
		UserIdentity userIdentity = request.getUserIdentity();
		tweetNode.setEmail(userIdentity.getEmail());
		tweetNode.setMobile(userIdentity.getMobile());
		tweetNode.setTagName(userIdentity.getTagName());
		tweetNode.setToken(userIdentity.getToken());
		return tweetNode;
	}
}
