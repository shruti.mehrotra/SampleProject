package com.shruti.twitter.neo4j.model.node;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import com.shruti.twitter.model.constants.SizeConstants;
import com.shruti.twitter.model.user.UserIdentity;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */

@Data
@NodeEntity
public class TweetNode {
	@GraphId
	private Long tweetId;
	private String heading;
	private List<File> attachments;
	private String content;
	private Date tweetTime;
	private String email;
	private String mobile;
	private String token;
	private String tagName;

//	private List<TweetReaction> reactions;
//	private Tweet tweet;
}
