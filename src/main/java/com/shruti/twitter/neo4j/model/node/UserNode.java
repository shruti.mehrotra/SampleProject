package com.shruti.twitter.neo4j.model.node;

import lombok.Data;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import com.shruti.twitter.model.user.User;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */

@Data
@NodeEntity
public class UserNode {
	@GraphId
	private Long userId;
	
	private User user;
}
