package com.shruti.twitter.neo4j.model.relation;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;
import org.springframework.stereotype.Component;

import com.shruti.twitter.neo4j.model.node.TweetNode;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */
@Data
@Component
@RelationshipEntity(type = "RETWEETS")
@EqualsAndHashCode(callSuper = false)
public class RetweetsRelation extends BaseRelationshipEntity {
	@StartNode
	private TweetNode tweet;
	
	@EndNode
	private TweetNode reTweet;
}
