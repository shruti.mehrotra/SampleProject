package com.shruti.twitter.neo4j.model.relation;

import lombok.Data;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;

@RelationshipEntity(type = "Test")
@Data
public abstract class BaseRelationshipEntity {
	@GraphId
	protected Long pathId;
}