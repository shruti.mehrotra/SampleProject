package com.shruti.twitter.neo4j;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */

@Configuration
@EnableNeo4jRepositories(basePackages = "com.shruti.twitter.neo4j.repo")
public class TwitterNeo4jConfiguration extends Neo4jConfiguration {
	@Value("${neo4j.driverClassName}")
	private String dbDriverClass;

	@Value("${neo4j.username}")
	private String dbUsername;

	@Value("${neo4j.password}")
	private String dbPassword;

	@Value("${neo4j.url}")
	private String dbURL;

	@Bean
	public org.neo4j.ogm.config.Configuration getConfiguration() {
		org.neo4j.ogm.config.Configuration config = new org.neo4j.ogm.config.Configuration();
		config.driverConfiguration().setDriverClassName(dbDriverClass).setCredentials(dbUsername, dbPassword)
				.setURI(dbURL);
		return config;
	}

	@Override
	public SessionFactory getSessionFactory() {
		return new SessionFactory(getConfiguration(), "com.shruti.twitter.neo4j.model");
	}
}
