package com.shruti.twitter.neo4j.repo.relation;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.shruti.twitter.neo4j.model.relation.FollowsRelation;
import com.shruti.twitter.neo4j.model.relation.MentionsRelation;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */

@Repository
@RepositoryRestResource
public interface MentionsRelationRepository extends GraphRepository<MentionsRelation> {
	@Query("MATCH (tweet:TweetNode)-[rel:MENTIONS]->(user:UserNode) WHERE user.name = {0} RETURN rel")
	List<FollowsRelation> findTweetByUser(String name);

	@Query("MATCH (user:UserNode)-[rel:MENTIONS]->(tweet:TweetNode) WHERE tweet.name = {0} RETURN rel")
	List<FollowsRelation> findUserByTweet(String name);
}
