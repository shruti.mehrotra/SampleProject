package com.shruti.twitter.neo4j.repo.relation;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.shruti.twitter.neo4j.model.relation.FollowsRelation;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */

@Repository
@RepositoryRestResource
public interface FollowsRelationRepository extends GraphRepository<FollowsRelation> {
	@Query("MATCH (follower:UserNode)-[rel:FOLLOWS]->(followed:UserNode) WHERE follower.name = {0} RETURN rel")
	List<FollowsRelation> findByFollower(String name);

	@Query("MATCH (follower:UserNode)-[rel:FOLLOWS]->(followed:UserNode) WHERE followed.name = {0} RETURN rel")
	List<FollowsRelation> findByFollowed(String name);
}
