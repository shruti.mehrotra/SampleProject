package com.shruti.twitter.neo4j.repo.relation;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.shruti.twitter.neo4j.model.relation.FollowsRelation;
import com.shruti.twitter.neo4j.model.relation.TagsRelation;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */

@Repository
@RepositoryRestResource
public interface TagsRelationRepository extends GraphRepository<TagsRelation> {
	@Query("MATCH (tweet:TweetNode)-[rel:TAGS]->(user:UserNode) WHERE user.name = {0} RETURN rel")
	List<FollowsRelation> findUserByTweet(String name);

	@Query("MATCH (user:UserNode)-[rel:TAGS]->(tweet:TweetNode) WHERE tweet.name = {0} RETURN rel")
	List<FollowsRelation> findTweetByUser(String name);
}
