package com.shruti.twitter.neo4j.repo.relation;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.shruti.twitter.neo4j.model.relation.FollowsRelation;
import com.shruti.twitter.neo4j.model.relation.RetweetsRelation;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */

@Repository
@RepositoryRestResource
public interface RetweetsRelationRepository extends GraphRepository<RetweetsRelation> {
	@Query("MATCH (tweet:TweetNode)-[rel:RETWEETS]->(retweet:TweetNode) WHERE retweet.name = {0} RETURN rel")
	List<FollowsRelation> findReTweetsByTweet(String name);

	@Query("MATCH (retweet:TweetNode)-[rel:RETWEETS]->(tweet:TweetNode) WHERE tweet.name = {0} RETURN rel")
	List<FollowsRelation> findTweetByReTweets(String name);
}
