package com.shruti.twitter.neo4j.repo.node;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import com.shruti.twitter.neo4j.model.node.UserNode;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 22-Dec-2016
 */

@Repository
public interface UserNodeRepository extends GraphRepository<UserNode> {

}
